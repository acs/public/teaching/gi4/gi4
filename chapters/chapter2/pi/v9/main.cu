#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>
#include <cuda.h>

#define BLOCK_DIM 32

long long num_steps = 1000000;
double step;

typedef struct {
    double sum;
    long long start, end;
} thread_param;


__global__
void pi_kernel(double *workspace, long long steps_per_thread, double step)
{
    __shared__ double sums[BLOCK_DIM];

    double x;
    long long i;
    long long start = (blockIdx.x*blockDim.x+threadIdx.x) * steps_per_thread;
    long long end = start + steps_per_thread;

    sums[threadIdx.x] = 0;
    for (i = start; i < end; i++) {
        x = (i + 0.5) * step;
        sums[threadIdx.x] += 4.0 / (1.0 + x * x);
    }
    __syncthreads();
    for (int i = 1; i < blockDim.x; i*=2) {
        if (threadIdx.x % (i*2) == 0) {
            sums[threadIdx.x] += sums[threadIdx.x+i];
        }
    }
    __syncthreads();
    if (threadIdx.x == 0) {
        workspace[blockIdx.x] = sums[0];
    }
}

int main(int argc, char **argv)
{
    double sum;
    int i;
    struct timeval start, end;

    if (argc > 1)
        num_steps = atoll(argv[1]);
    if (num_steps < 100)
        num_steps = 1000000;
    printf("\nnum_steps = %lld\n", num_steps);
    cuInit(0);
    gettimeofday(&start, NULL);

    sum = 0.0;
    step = 1.0 / (double)num_steps;

    double *dev_workspace;
    double host_workspace[512];
    dim3 threadsPerBlock(BLOCK_DIM);
    dim3 numBlocks(512);
    cudaError_t err;

    if ((err = cudaMalloc(&dev_workspace, 512*sizeof(double))) != cudaSuccess) {
        printf("error: %s\n", cudaGetErrorString(err));
        return 1;
    }

    pi_kernel<<<numBlocks, threadsPerBlock>>>(
        dev_workspace,
        num_steps/(threadsPerBlock.x*numBlocks.x),
        step);

    if ((err = cudaMemcpy(host_workspace, dev_workspace, numBlocks.x*sizeof(double), cudaMemcpyDeviceToHost)) != cudaSuccess) {
        printf("error: %s\n", cudaGetErrorString(err));
        return 1;
    }

    /* Wait until all threads have terminated
       and calculate PI */
    for (i = 0; i < numBlocks.x; i++) {
        sum += host_workspace[i];
    }

    gettimeofday(&end, NULL);
    printf("PI = %f\n", sum * step);
    printf("Time : %lf sec\n", (double)(end.tv_sec-start.tv_sec)+(double)(end.tv_usec-start.tv_usec)/1000000.0);

    return 0;
}

